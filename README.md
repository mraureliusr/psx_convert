# psx_convert
Hacking up a module to use a PSX controller on the Intellivision!

Adding a README in case anyone ever looks at this code, lol.
So I wanted to be able to use a PlayStation controller to play Intellivision games on my Intellivision. Obviously many games are optimized for the keypad style controller of the Intellivision, but there are many games where using the analog joysticks will be very nice.
I never completed the project due to my rudimentary understanding of circles at the time. Perhaps I will come back to it sometime.
