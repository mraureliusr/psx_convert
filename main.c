/* 
 * File:   main.c
 * Author: amrowsell
 *
 * With any luck, we'll be able to use a PSX controller on
 * an Intellivision! How freaking awesome is that!
 *
 * With the PSX controller bus reverse-engineered (it's basically an SPI bus),
 * we can now start to implement the controller interface half of the code.
 * The step after this will be to implement the Intv interface code.
 * 
 * Created on June 24, 2015, 11:23 PM
 */

#include <xc.h>
#include "configbits.h"
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

/*
 * System clock is 24MHz
 * Peripheral clock is 3MHz
 *
 */

typedef struct {
	unsigned char select: 1;
	unsigned char R3: 1;
	unsigned char L3: 1;
	unsigned char start: 1;
	unsigned char dUp: 1;
	unsigned char dDown: 1;
	unsigned char dRight: 1;
	unsigned char dLeft: 1;
	unsigned char L2: 1;
	unsigned char R2: 1;
	unsigned char L1: 1;
	unsigned char R1: 1;
	unsigned char triangle: 1;
	unsigned char circle: 1;
	unsigned char cross: 1;
	unsigned char square: 1;

	unsigned char JoyR_H;
	unsigned char JoyR_V;
	unsigned char JoyL_H;
	unsigned char JoyL_V;

	unsigned char segmentRight;
	unsigned char segmentLeft;


} PSX_t;

#define PSX_DIGITIAL 0x41
#define PSX_ANALOG 0x73
#define PI_ON_EIGHT 0.3926991
double cot(double angle) {
	return 1.0 / tan(angle);
}
unsigned char endianSwap(unsigned char b) {
	uint64_t result;
	result = ((b * 0x80200802ULL) & 0x0884422110ULL) * 0x0101010101ULL >> 32;
	return (unsigned char)result;
}
void waitForAck(void) {
	unsigned char temp;
	while(!IFS1bits.CNBIF); // wait for ack
	temp = PORTB; // read port to clear mismatch
	IFS1bits.CNBIF = 0; // clear flag
	return;
}
void setupSPI(void) {
	char readPort;
	// set up ACK pin on RB1 (pin 5), change notification for falling edge
	TRISBbits.TRISB1 = 1; // set as input
	ANSELBbits.ANSB1 = 0; // clear analog
	CNCONBbits.ON = 1; // enable change notification
	CNENBbits.CNIEB1 = 1; // enable for PB1
	//CNPUBbits.CNPUB1 = 1;	// enable pull-up
	readPort = PORTB; // read bit to clear mismatch
	IFS1bits.CNBIF = 0; // clear flag
	// CS is on pin 26
	ANSELBbits.ANSB15 = 0; // clear analog bit
	TRISBbits.TRISB15 = 0; // set as output
	LATBbits.LATB15 = 1; // set high
	// defaults to 8-bit mode, SS disabled (controlled by port)
	// SPI clock is generated from SPI1BRG
	RPB13R |= 0x03; // SDO on RPB13 (pin 24)
	SDI1R |= 0x04; // SDI on RPB8 (pin 17)
	SPI1CONbits.ENHBUF = 1; // turn on enhanced buffer
	SPI1BRG = 5; // set baud rate gen to 250kHz
	SPI1CONbits.MSTEN = 1;
	SPI1CONbits.CKP = 1;
	SPI1CONbits.CKE = 0;

	SPI1CONbits.ON = 1; // activate peripheral
}
void dirJoystick(PSX_t *controller) {
	double a;
	int8_t x, y;

	// first section will deal with right joystick
	x = controller->JoyR_H - 128; // shift from 0-255 to -128-127
	y = controller->JoyR_V - 128;

	if((x < 10 && x > -10) && (y < 10 && y > -10))
		controller->segmentRight = 8; // centered
	else {
		if((abs(y) - abs(x)) > 0) {
			a = atan(abs(x / y));
			if(a < PI_ON_EIGHT && y > 0)
				controller->segmentRight = 2;
			else if(a < PI_ON_EIGHT && y < 0)
				controller->segmentRight = 6;
		} else if((abs(y) - abs(x)) < 0) {
			a = atan(abs(y / x));
			if(a < PI_ON_EIGHT && x > 0)
				controller->segmentRight = 0;
			else if(a < PI_ON_EIGHT && x < 0)
				controller->segmentRight = 4;
		} else {
			if(x > 0 && y > 0)
				controller->segmentRight = 1;
			else if(x < 0 && y > 0)
				controller->segmentRight = 3;
			else if(x < 0 && y < 0)
				controller->segmentRight = 5;
			else if(x > 0 && y < 0)
				controller->segmentRight = 7;
		}
	}
	// now left joystick
	a = 0;
	x = controller->JoyL_H - 128; // shift from 0-255 to -128-127
	y = controller->JoyL_V - 128;
	if((x < 10 && x > -10) && (y < 10 && y > -10)) // if it's close to center...
		controller->segmentLeft = 8; // centered
	else {
		if((abs(y) - abs(x)) > 0) {
			a = atan(abs(x / y));
			if(a < PI_ON_EIGHT && y > 0)
				controller->segmentLeft = 2;
			else if(a < PI_ON_EIGHT && y < 0)
				controller->segmentLeft = 6;
		} else if((abs(y) - abs(x)) < 0) {
			a = atan(abs(y / x));
			if(a < PI_ON_EIGHT && x > 0)
				controller->segmentLeft = 0;
			else if(a < PI_ON_EIGHT && x < 0)
				controller->segmentLeft = 4;
		} else {
			if(x > 0 && y > 0)
				controller->segmentLeft = 1;
			else if(x < 0 && y > 0)
				controller->segmentLeft = 3;
			else if(x < 0 && y < 0)
				controller->segmentLeft = 5;
			else if(x > 0 && y < 0)
				controller->segmentLeft = 7;
		}
	}
}
void checkController(PSX_t *controller) {
	unsigned char y, q, type, temp;

	LATBbits.LATB15 = 0; // set CS low
	simpleDelayMicro(2);
	y = 0x01;
	SPI1BUF = endianSwap(y);
	while(SPI1STATbits.SPIBUSY); // wait for transaction to complete
	y = SPI1BUF; // clear receive buffer
	waitForAck();
	simpleDelayMicro(5);
	y = 0x42;
	y = endianSwap(y);
	SPI1BUF = y;
	while(SPI1STATbits.SPIBUSY); // wait for transaction to complete
	waitForAck();
	simpleDelayMicro(5);
	type = SPI1BUF;
	type = endianSwap(type);
	SPI1BUF = 0x00;
	while(SPI1STATbits.SPIBUSY); // wait for transaction to complete
	waitForAck();
	simpleDelayMicro(5);
	temp = SPI1BUF;
	while(SPI1STATbits.SPIBUSY); // wait for transaction to complete
	temp = endianSwap(temp);
	if(temp == 0x5A) {
		// here comes the data
		// we'll fill the buffer with six bytes
		for(q = 0; q < 6; q++) {
			SPI1BUF = 0x00;
			if(q == 5)
				break; // last byte won't ACK
			waitForAck();
			//while(SPI1STATbits.SPIBUSY) ; // wait for transaction to complete
			simpleDelayMicro(31);
		}
		while(SPI1STATbits.SPIBUSY); // wait for exchange to complete
		LATBbits.LATB15 = 1; // set CS high
		// now we need to decode all the bytes we got into the PSX_t struct
		temp = SPI1BUF; // retrieve first byte
		controller->select = (temp & 0x80) >> 7;
		controller->R3 = (temp & 0x40) >> 6;
		controller->L3 = (temp & 0x20) >> 5;
		controller->start = (temp & 0x10) >> 4;
		controller->dUp = (temp & 0x08) >> 3;
		controller->dRight = (temp & 0x04) >> 2;
		controller->dDown = (temp & 0x02) >> 1;
		controller->dLeft = (temp & 0x01);
		temp = SPI1BUF; // retrieve second byte
		controller->L2 = (temp & 0x80) >> 7;
		controller->R2 = (temp & 0x40) >> 6;
		controller->L1 = (temp & 0x20) >> 5;
		controller->R1 = (temp & 0x10) >> 4;
		controller->triangle = (temp & 0x08) >> 3;
		controller->circle = (temp & 0x04) >> 2;
		controller->cross = (temp & 0x02) >> 1;
		controller->square = (temp & 0x01);
		temp = SPI1BUF;
		temp = endianSwap(temp);
		controller->JoyR_H = temp;
		temp = SPI1BUF;
		temp = endianSwap(temp);
		controller->JoyR_V = temp;
		temp = SPI1BUF;
		temp = endianSwap(temp);
		controller->JoyL_H = temp;
		temp = SPI1BUF;
		temp = endianSwap(temp);
		controller->JoyL_V = temp;
		
		dirJoystick(controller);

	} else {
		// something went wrong, end transmission
		LATBbits.LATB15 = 1; // set CS high
	}
}
void setupTimer(void) {
	RPB0R |= 0x05; // setup RPB0 as OC3
	PR2 = 0x01FF;
	OC3RS = 0x004E;
	OC3CON |= 0x8006; // PWM mode
	T2CONSET = 0x8000;
}
int main(void) {
	DDPCONbits.JTAGEN = 0; // disable JTAG so we can use I2C1
	PSX_t controllerOne, *pController = &controllerOne;
	//setupTimer();
	setupSPI();
	checkController(pController);
	return (EXIT_SUCCESS);
}

